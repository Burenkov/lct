from abc import ABC, abstractmethod
from gigachat import GigaChat
from gigachat.models import Chat, Messages, MessagesRole
import os
from slugify import slugify
import json
import re
from datetime import datetime
from django.conf import settings


DEBUG = False

class BaseAIProvider(ABC):
    @abstractmethod
    def get_content(self, input):
        pass


class GigaChatProvider(BaseAIProvider):
    client_id = settings.GIGA_CHAT_CLIENT_ID
    auth_token = settings.GIGA_CHAT_AUTH_TOKEN

    def _get_content(self, input):
        payload = Chat(
            messages=[
                Messages(
                    role=MessagesRole.SYSTEM,
                    content="""тебе на вход подается сообщение пользователя в котором надо понять один из сценарьев:
                    1. пользователь хочет получить прогноз по закупке товара на определенный период из него надо получить название продукта, понять на период числом. в ответе отдай json с полями product и year, и type со значением forecast.
                    2. пользователь хочет купить несколько товаров нужно на определенный период из него надо получить название продуктов, понять на период числом. в ответе отдай json с полями products и year, и type со значением many.
                    3. пользователь хочет запросить остатки на складе по нескольким товарам в ответе отдай json с полями products и type со значением warehouse.
"""
#                     content="""тебе на вход подается сообщение пользователя в котором он хочет получить прогноз по закупке товара на определенный период. 
# из него надо получить название продукта, понять на период числом. в ответе отдай json с полями product и year.
# """
                )
            ],
        )
        payload.messages.append(Messages(role=MessagesRole.USER, content=input))
        with GigaChat(credentials=self.auth_token, scope="GIGACHAT_API_PERS", verify_ssl_certs=False) as giga:
            response = giga.chat(payload)
            content = response.choices[0].message.content
            return content

    def get_content(self, input):
        if DEBUG:
            return "{\"product\": \"Скрепки металлические\", \"year\": 2024}"
        else:
            return self._get_content(input)

gpt = GigaChatProvider()