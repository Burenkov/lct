from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from ..models import Message, Calculation
from ..serializers import MessageSerializer
from django.db.models import Q
from contract.models import Product, Contract
from django.db.models import Count, Sum

import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller
from statsmodels.tsa.arima.model import ARIMA
import numpy as np
import json
from django.utils import timezone
from ..services import gpt
from product.services.restriction import restriction_groups
from warehouse.models import Product as ProductWarehouse, Remains

def get_product(name):
    return Product.objects.filter(Q(title__icontains=name) | Q(warehouse__title__icontains=name)).first()

def get_product_info(name):
    p = get_product(name)
    if p is None:
        return [], [], None
    pk = p.pk
    # objs = (Contract.objects.filter(product__id=pk).values('date_last_change', 'price')
    #     .annotate(count=Count('date_last_change')).order_by('date_last_change'))
    objs = Contract.objects.filter(
        product__id=pk
    ).values('date_last_change__year', 'date_last_change__month', 'price') \
        .annotate(total_price=Sum('price')).order_by('date_last_change__year', 'date_last_change__month')
    dates = []
    counts = []
    for el in objs:
        dates.append(f"{el['date_last_change__year']}-{el['date_last_change__month']}-01")
        counts.append(int(el['total_price']))
    return dates, counts, p

def get_arima(dates, counts, year):
    # Создание DataFrame
    sales_df = pd.DataFrame({'days': dates, 'values': counts})

    # Преобразование в datetime
    sales_df['days'] = pd.to_datetime(sales_df['days'])

    # Устанавливаем 'days' в качестве индекса
    # sales_df.set_index('days', inplace=True)

    # Выводим первые несколько строк
    model = ARIMA(sales_df['values'], order=(1, 1, 1))
    model_fit = model.fit()

    # Прогноз на будущее (следующие 12 месяцев)
    forecast_future = model_fit.forecast(steps=12)

    # Создаем новый DataFrame для будущих значений
    future_dates = pd.date_range(start=f'{year}-01-01', periods=12, freq='ME')
    forecast_df = pd.DataFrame({'days': future_dates, 'values': forecast_future})

    # Присоединяем прогноз к исходному DataFrame
    sales_df = sales_df._append(forecast_df, ignore_index=True)

    return sales_df.to_json()

def get_arima_future(dates, counts):
    # Создание DataFrame
    sales_df = pd.DataFrame({'days': dates, 'values': counts})

    # Преобразование в datetime
    sales_df['days'] = pd.to_datetime(sales_df['days'])

    # Устанавливаем 'days' в качестве индекса
    # sales_df.set_index('days', inplace=True)

    # Выводим первые несколько строк
    model = ARIMA(sales_df['values'], order=(1, 1, 1))
    model_fit = model.fit()

    # Прогноз на будущее (следующие 12 месяцев)
    forecast_future = model_fit.forecast(steps=12)

    # Создаем новый DataFrame для будущих значений
    future_dates = pd.date_range(start='2024-01-01', periods=12, freq='ME')
    forecast_df = pd.DataFrame({'days': future_dates, 'values': forecast_future})

    # Присоединяем прогноз к исходному DataFrame
    # sales_df = sales_df._append(forecast_df, ignore_index=True)

    return forecast_df.to_dict()

class ChatViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Message.objects.filter(
            Q(sender=self.request.user) | Q(recipient=self.request.user) | Q(type=Message.Type.ALL)
        ).distinct()

    @action(methods=['POST'], detail=False)
    def calculation(self, request) -> Response:
        products = request.data.get('product', '').split(',')
        address = request.data.get('address', '')
        user = request.user
        _products_for_group = []
        for name in products:
            _product = get_product(name)
            _main_product = _product.get_main_product_for_spgz()
            if _main_product is not None:
                _products_for_group.append([_main_product, name])
        groups = restriction_groups(_products_for_group)
        for grop in groups:
            rows = []
            product_names = []
            for elements in grop:
                name = elements[1]
                product_names.append(name)
                dates, counts, product = get_product_info(name)
                data = get_arima_future(dates, counts)
                days = data['days']
                values = data['values']
                # main_product = product.get_main_product_for_spgz()
                for key, day in days.items():
                    value = values[key]
                    end_date = str(day)
                    year = day.year
                    # start_date = 
                    res = {
                        "DeliverySchedule": { # График поставки
                            "dates": {
                                "end_date": end_date, # Дата окончания поставки – данные рождаются в процессе прогнозирования
                                "start_date": " " # Дата начала поставки– данные рождаются в процессе прогнозирования
                            },
                            "deliveryAmount": value, # Объем поставки– данные рождаются в процессе прогнозирования
                            "deliveryConditions": "", # Условия поставки– данные рождаются в процессе прогнозирования
                            "year": year # Год– данные рождаются в процессе прогнозирования
                        },
                        "address": { # Адрес поставки– данные рождаются в процессе прогнозирования
                            "gar_id": " ", # Идентификатор ГАР – это федеральный справочник адресов
                            "text": address # Адрес в текстовой форме – если не нашли ГАР – можно использовать это полне
                        },
                        "entityId": product.get_id_spgz(), # Сквозной идентификатор СПГЗ – данные рождаются в процессе прогнозирования
                        "id": "", # Идентификатор (версии) СПГЗ– данные рождаются в процессе прогнозирования
                        "nmc": "", # Сумма спецификации – данные рождаются в процессе прогнозирования, если получится еще и цену предсказать
                        "okei_code": " ", # Ед. измерения по ОКЕИ – данные рождаются в процессе прогнозирования
                        "purchaseAmount":  "",# Объем поставки – данные рождаются в процессе прогнозирования
                        # Характеристики СПГЗ. Заполняются в зависимости от типа характеристики в соответствии со структурой справочника СПГЗ – данные определяются в процессе прогнозирования и базируются на выгрузке СПГЗ, КПГЗ
                        "spgzCharacteristics": [{
                            "characteristicName": " ",
                            "characteristicSpgzEnums": [{ "value": " "}],
                            "conditionTypeId":  "",
                            "kpgzCharacteristicId": "",
                            "okei_id": "",
                            "selectType": "",
                            "typeId": "",
                            "value1": "",
                            "value2": ""
                        }]
                    }
                    rows.append(res)
            print(product_names, 'product_names')
            product_text = ', '.join(product_names)
            Calculation.objects.create(
                title=f'Прогноз по продуктам {product_text}',
                customer_id=user.uuid,
                rows=rows,
                company=user.get_company()
            )
        return Response({'status': 'ok'})

    @action(methods=['POST'], detail=False)
    def send(self, request) -> Response:
        serializer = MessageSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user_message = serializer.save()
        type = 'forecast'
        gpt_res = {}
        try:
            gpt_res_text = gpt.get_content(user_message.text)
            gpt_res = json.loads(gpt_res_text)
        except Exception as e:
            system_message = Message.objects.create(
                recipient=self.request.user,
                text=f"Не удалось обработать сообщение. Ошибка.",
            )
            return Response({ "data": [serializer.data, MessageSerializer(system_message).data] })
        type = gpt_res['type']
        final_data = [serializer.data]
        if user_message.text == 'Вход в систему':
            r = Remains.objects.all().order_by('-updated_at').first()
            system_message = Message.objects.create(
                recipient=self.request.user,
                text=f'Добро пожаловать в систему {self.request.user}. Данные по остаткам загружались последний раз {r.updated_at.strftime("%d.%m.%Y %H:%M:%S")}',
            )
            return Response({ "data": final_data })
        if type == 'forecast' and gpt_res['product'] and gpt_res['year']:
            product_title = gpt_res['product']
            dates, counts, p = get_product_info(product_title)
            if p is not None:
                data = get_arima(dates, counts, gpt_res['year'])
                system_message = Message.objects.create(
                    recipient=self.request.user,
                    text=f"Прогноз на будущей год {gpt_res['year']} по продукту - {product_title}",
                    system_info={
                        'diagram': json.loads(data),
                        'product_title': product_title,
                        'gpt_res': gpt_res
                    }
                )
                final_data.append(MessageSerializer(system_message).data)
            else:
                system_message = Message.objects.create(
                    recipient=self.request.user,
                    text=f"Продукту {product_title} не найден в базе данных для составления анализа",
                )
                final_data.append(MessageSerializer(system_message).data)
        elif type == 'many' and gpt_res['products'] and gpt_res['year']:
            many_products_find = []
            for product_title in gpt_res['products']:
                dates, counts, p = get_product_info(product_title)
                if p is not None:
                    data = get_arima(dates, counts, gpt_res['year'])
                    system_message = Message.objects.create(
                        recipient=self.request.user,
                        text=f"Прогноз на будущей год {gpt_res['year']} по продукту - {product_title}",
                        system_info={
                            'diagram': json.loads(data),
                            'product_title': product_title,
                            'gpt_res': gpt_res
                        }
                    )
                    final_data.append(MessageSerializer(system_message).data)
                    many_products_find.append(product_title)
                else:
                    system_message = Message.objects.create(
                        recipient=self.request.user,
                        text=f"Продукту {product_title} не найден в базе данных для составления анализа",
                    )
                    final_data.append(MessageSerializer(system_message).data)
            if len(many_products_find) > 0:
                system_message = Message.objects.create(
                    recipient=self.request.user,
                    text=f"Хотите сформировать закупку по продуктам - {', '.join(many_products_find)}?",
                    system_info={
                        'many_products_find': many_products_find
                    }
                )
                final_data.append(MessageSerializer(system_message).data)
        elif type == 'warehouse' and gpt_res['products']:
            filters = Q()
            for el in gpt_res['products']:
                filters |= Q(title__icontains=el)
            pw = ProductWarehouse.objects.filter(filters)
            text = ""
            for item in pw:
                text += f"- {item.title} ({item.count()} шт.)\n"
            system_message = Message.objects.create(
                recipient=self.request.user,
                text=text,
            )
            final_data.append(MessageSerializer(system_message).data)
        return Response({ "data": final_data })
