from rest_framework import viewsets
from rest_framework import filters
from ..models import Calculation
from ..serializers import CalculationSerializer


class ContractViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Calculation.objects.all()
    serializer_class = CalculationSerializer
    filter_backends = [filters.SearchFilter]
