from rest_framework import serializers
from ..models import Message

class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )
    type = serializers.ChoiceField(choices=Message.Type, default=Message.Type.USER)

    class Meta:
        model = Message
        read_only_fields = ('created_at', 'recipient', 'type',)
        fields = (
            'pk',
            'text',
            'created_at',
            'sender',
            'recipient',
            'type',
            'system_info',
        )
