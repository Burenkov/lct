from rest_framework import serializers
from ..models import Calculation

class CalculationSerializer(serializers.ModelSerializer):

    export_data = serializers.SerializerMethodField()

    def get_export_data(self, obj):
        return obj.export()

    class Meta:
        model = Calculation
        fields = (
            'id',
            'title',
            'uuid',
            'lot_entity_id',
            'customer_id',
            'rows',
            'company',
            'export_data',
        )
