import uuid
from django.db import models

class Calculation(models.Model):
    title = models.CharField(max_length=255, blank=True, default='', verbose_name='Название расчета')
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, verbose_name='Уникальный идентификатор')
    lot_entity_id = models.CharField(max_length=255, blank=True, default='', verbose_name="Идентификатор лота")
    customer_id = models.UUIDField(null=True, blank=True, verbose_name='Идентификатор заказчика')
    rows = models.JSONField(null=True, blank=True, verbose_name='Строки прогноза')
    company = models.ForeignKey('user.Company', on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Компания')

    def export(self):
        return {
            "id": self.uuid, # идетнификатор расчета – присваивается вашим решением при формировании расчета
            "lotEntityId": self.lot_entity_id, # Идентификатор лота – пока не матчить, нужен будет позже для интеграций с другими системами 
            "CustomerId":  self.customer_id, # Идентификатор заказчика – надо матчить с идентификатором заказчика, который должен быть определен по пользователю, взаимодействующему с ботом. Если нет возможности подставить реальный, просьба продемонстрировать возможность такой подстановки.
            "rows": self.rows
        }

    class Meta:
        verbose_name = "Расчёт"
        verbose_name_plural = "Расчёты"
        ordering = ('-id',)

    def __str__(self):
        return str(self.uuid)
