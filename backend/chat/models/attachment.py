from django.db import models

class Attachment(models.Model):

    class Meta:
        verbose_name = "Вложение"
        verbose_name_plural = "Вложения"

    file = models.FileField(upload_to="chat/attachment/", verbose_name="Файл")
    message = models.ForeignKey("chat.Message", on_delete=models.CASCADE, verbose_name="Сообщение")

    def __str__(self):
        return self.file.name