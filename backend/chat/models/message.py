from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Message(models.Model):
    class Type(models.TextChoices):
        USER = 'USER', 'пользователь'
        SYSTEM = 'SYSTEM', 'Система'
        ALL = 'ALL', 'Все'

    text = models.TextField(verbose_name='Тест')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    sender = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Отправитель')
    recipient = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='recipient', verbose_name='Получатель')
    type = models.CharField(
        max_length=10,
        choices=Type.choices,
        default=Type.SYSTEM,
        verbose_name='Тип'
    )
    system_info = models.JSONField(null=True, blank=True, verbose_name='Системная информация')

    class Meta:
        ordering = ('-created_at',)
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return self.text
