from rest_framework import routers
from .views import ChatViewSet, ContractViewSet


router = routers.SimpleRouter()
router.register('message', ChatViewSet, basename='chat')
router.register('calculation', ContractViewSet, basename='calculation')

urlpatterns = router.urls
