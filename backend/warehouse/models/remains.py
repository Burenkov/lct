import datetime
from django.db import models
from .category import Category
from .product import Product

class Remains(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')
    inventory_number = models.CharField(max_length=255, verbose_name='Инвентарный номер', default='')
    okof = models.CharField(max_length=255, verbose_name='ОКОФ', default='')
    depreciation_group = models.CharField(max_length=255, verbose_name='Амортизационная группа', default='')
    depreciation_method = models.CharField(max_length=255, verbose_name='Амортизационный метод', default='')
    date_acceptance_for_accounting = models.DateField(verbose_name='Дата приемки для учета', null=True, blank=True, default=datetime.date.today)
    condition = models.CharField(max_length=255, verbose_name='Состояние', default='')
    useful_life = models.CharField(max_length=255, verbose_name='Срок полезного использования', default='')
    monthly_wear_rate = models.CharField(max_length=255, verbose_name='Мес. норма износа, %', default='')
    wear = models.DecimalField(max_length=255, verbose_name='Износ, %', default=0.0, max_digits=10, decimal_places=2)
    balance_value = models.DecimalField(max_length=255, verbose_name='Балансовая стоимость', default=0.0, max_digits=10, decimal_places=2)
    quantity = models.IntegerField(verbose_name='Количество', default=0)
    amount_depreciation = models.DecimalField(max_length=255, verbose_name='Сумма амортизации', default=0.0, max_digits=10, decimal_places=2)
    residual_value = models.DecimalField(max_length=255, verbose_name='Остаточная стоимость', default=0.0, max_digits=10, decimal_places=2)
    category = models.ForeignKey(
        Category, 
        null=True, 
        blank=True, 
        on_delete=models.SET_NULL, 
        verbose_name='Категория'
    )
    product = models.ForeignKey(
        Product, 
        null=True, 
        blank=True, 
        on_delete=models.SET_NULL, 
        verbose_name='Продукт'
    )
    balance_of_date = models.DateField(verbose_name='Баланс на дату', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата/время создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата/время изменения')

    class Meta:
        verbose_name = 'Остатки'
        verbose_name_plural = 'Остатки'
        ordering = ('balance_of_date', 'title',)

    def __str__(self) -> str:
        return f'{self.title}'
