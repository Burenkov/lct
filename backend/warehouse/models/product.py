from django.db import models
from django.db.models import Count

class Product(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')

    def count(self):
        objs = (self.remains_set.filter(product__id=self.pk).values('balance_of_date')
                .annotate(count=Count('balance_of_date')).order_by('balance_of_date'))
        if len(objs) == 0:
            return 0
        return objs[0]['count']

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title