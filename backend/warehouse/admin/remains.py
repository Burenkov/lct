from django.contrib import admin
from ..models import Remains

@admin.register(Remains)
class RemainsAdmin(admin.ModelAdmin):
    search_fields = ['title',]
    list_display = ['title', 'quantity',]
