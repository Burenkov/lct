# Generated by Django 3.2.25 on 2024-06-08 14:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('warehouse', '0004_auto_20240608_1715'),
    ]

    operations = [
        migrations.RenameField(
            model_name='remains',
            old_name='property',
            new_name='product',
        ),
    ]
