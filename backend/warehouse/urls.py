from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'product', views.ProductViewSet, basename='project_files')

urlpatterns = router.urls
