import re
import os
from decimal import Decimal
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from openpyxl import load_workbook
from ...models import Remains, Category, Product


def validate_category(string):
    pattern = re.compile(r"^[0-9]+(.[0-9]+)?, .*$")
    return pattern.match(string) is not None

class Command(BaseCommand):
    def handle(self, *args, **options):
        full_path = os.path.realpath(__file__)
        
        path = os.path.dirname(full_path)
        files = [
            # 'Ведомость остатков на 30.06.2022г. (сч. 21).xlsx',
            # 'Ведомость остатков на 30.06.2022г. (сч. 101).xlsx',
            # 'Ведомость остатков на 30.09.2022г. (сч. 21).xlsx',
            # 'Ведомость остатков на 30.09.2022г. (сч. 101).xlsx',
            # 'Ведомость остатков на 31.03.2022г. (сч. 21).xlsx',
            # 'Ведомость остатков на 31.03.2022г.(сч. 101).xlsx',
            # 'Ведомость остатков на 31.12.2022г. (сч. 21).xlsx',
            # 'Ведомость остатков на 31.12.2022г. (сч. 101).xlsx',
        ]
        for file in files:
            # Ищем дату формата ДД.ММ.ГГГГ
            date_pattern = r"(\d{2}\.\d{2}\.\d{4})"
            match = re.search(date_pattern, file)
            if match:
                _date = match.group(1)
            else:
                _date = None
            balance_of_date = datetime.strptime(_date, '%d.%m.%Y').date()
            print(f'{path}/data/{file}')
            wb = load_workbook(f'{path}/data/{file}')
            sheet = wb.active
            current_category = None
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if validate_category(str(value)):
                        category, created = Category.objects.get_or_create(title=value.strip())
                        current_category = category
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                number = obj_data[0]
                title = obj_data[2]
                inventory_number = obj_data[9]
                okof = '' if obj_data[10] is None else obj_data[10]
                if number is not None and number.replace(' ', '').isdigit() and title is not None and inventory_number is not None:
                    balance_value = Decimal(0) if obj_data[19] is None else Decimal(obj_data[19])
                    product, _ = Product.objects.get_or_create(title=title)
                    remains, created = Remains.objects.update_or_create(
                        inventory_number=inventory_number,
                        balance_of_date=balance_of_date,
                        # okof=okof,
                        defaults={
                            'title': obj_data[2],
                            'inventory_number': obj_data[9],
                            'okof': okof,
                            'depreciation_group': '' if obj_data[12] is None else obj_data[12],
                            'depreciation_method': str('' if obj_data[13] is None else obj_data[13]),
                            'date_acceptance_for_accounting': None if obj_data[14] is None else datetime.strptime(obj_data[14], '%d.%m.%Y').date(),
                            'condition': obj_data[15],
                            'useful_life': str('' if obj_data[16] is None else obj_data[16]),
                            'monthly_wear_rate': obj_data[17],
                            'wear': Decimal(0 if obj_data[18] is None or obj_data[18] == '-' else obj_data[18]),
                            'balance_value': balance_value,
                            'quantity': int(0 if obj_data[20] is None else obj_data[20]),
                            'amount_depreciation': Decimal(0 if obj_data[21] is None else obj_data[21]),
                            'residual_value': Decimal(0 if obj_data[22] is None else obj_data[22]),
                            'category': current_category,
                            'product': product,
                            'balance_of_date': balance_of_date
                        }
                    )
                    # if created is False:
                    #     remains.product = product
                    #     remains.save()

        
        
        files_105 = [
            'Ведомость остатков на 31.03.2022 (сч. 105).xlsx',
            'Ведомость остатков на 30.06.2022 (сч. 105).xlsx',
            'Ведомость остатков на 30.09.2022 (сч. 105).xlsx',
            'Ведомость остатков на 31.12.2022г. (сч. 105).xlsx',
        ]
        for file in files_105:
            date_pattern = r"(\d{2}\.\d{2}\.\d{4})"
            match = re.search(date_pattern, file)
            if match:
                _date = match.group(1)
            else:
                _date = None
            balance_of_date = datetime.strptime(_date, '%d.%m.%Y').date()
            print(f'{path}/data/{file}')
            wb = load_workbook(f'{path}/data/{file}')
            sheet = wb.active
            current_category = None
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if validate_category(str(value)):
                        category, created = Category.objects.get_or_create(title=value.strip())
                        current_category = category
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                print(obj_data, 'obj_data')
                # number = obj_data[0]
                # title = obj_data[2]
                # inventory_number = obj_data[9]
                # okof = '' if obj_data[10] is None else obj_data[10]
                # if number is not None and number.replace(' ', '').isdigit() and title is not None and inventory_number is not None:
                #     balance_value = Decimal(0) if obj_data[19] is None else Decimal(obj_data[19])
                #     product, _ = Product.objects.get_or_create(title=title)
                #     remains, created = Remains.objects.update_or_create(
                #         inventory_number=inventory_number,
                #         balance_of_date=balance_of_date,
                #         # okof=okof,
                #         defaults={
                #             'title': obj_data[2],
                #             'inventory_number': obj_data[9],
                #             'okof': okof,
                #             'depreciation_group': '' if obj_data[12] is None else obj_data[12],
                #             'depreciation_method': str('' if obj_data[13] is None else obj_data[13]),
                #             'date_acceptance_for_accounting': None if obj_data[14] is None else datetime.strptime(obj_data[14], '%d.%m.%Y').date(),
                #             'condition': obj_data[15],
                #             'useful_life': str('' if obj_data[16] is None else obj_data[16]),
                #             'monthly_wear_rate': obj_data[17],
                #             'wear': Decimal(0 if obj_data[18] is None or obj_data[18] == '-' else obj_data[18]),
                #             'balance_value': balance_value,
                #             'quantity': int(0 if obj_data[20] is None else obj_data[20]),
                #             'amount_depreciation': Decimal(0 if obj_data[21] is None else obj_data[21]),
                #             'residual_value': Decimal(0 if obj_data[22] is None else obj_data[22]),
                #             'category': current_category,
                #             'product': product,
                #             'balance_of_date': balance_of_date
                #         }
                #     )
        self.stdout.write(
            self.style.SUCCESS('Successfully closed')
        )
