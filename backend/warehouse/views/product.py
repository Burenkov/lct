from rest_framework import viewsets
from rest_framework import filters
from ..models import Product, Remains
from ..serializers import ProductSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
from django.db.models import Count


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']

    @action(
        detail=True,
        methods=['get'],
    )
    def remains(self, request, pk=None):
        objs = (Remains.objects.filter(product__id=pk).values('balance_of_date')
                .annotate(count=Count('balance_of_date')).order_by('balance_of_date'))
        return Response({
            'remains': list(objs),
        })
