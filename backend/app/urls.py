from garpixcms.urls import *  # noqa
from django.conf import settings

API_URL = getattr(settings, 'API_URL', 'api')

urlpatterns = [
    path(f'{API_URL}/warehouse/', include('warehouse.urls')),   # noqa: F405
    path(f'{API_URL}/user/', include('user.urls')),   # noqa: F405
    path(f'{API_URL}/chat/', include('chat.urls')),   # noqa: F405
    path(f'{API_URL}/contract/', include('contract.urls')),   # noqa: F405
] + urlpatterns  # noqa
