from garpixcms.settings import *  # noqa

AUTHENTICATION_BACKENDS = [
    'garpix_user.utils.backends.CustomAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
    # 'rest_framework_social_oauth2.backends.DjangoOAuth2',
]

MIGRATION_MODULES.update({  # noqa:F405
    'fcm_django': 'app.migrations.fcm_django',
})

INSTALLED_APPS += [
    'django_filters',
    'warehouse',
    'contract',
    'chat',
    'product',
]

GARPIX_ACCESS_TOKEN_TTL_SECONDS = 86400      # 1 day
GARPIX_REFRESH_TOKEN_TTL_SECONDS = 15552000  # 6 months

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'garpix_user.rest.authentication.MainAuthentication',
        'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
        'rest_framework_social_oauth2.authentication.SocialAuthentication',
    ),
    'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend']
}

CORS_ORIGIN_ALLOW_ALL = True

CORS_ORIGIN_WHITELIST = (
    'http://localhost:5173',
)

GIGA_CHAT_CLIENT_ID = env('GIGA_CHAT_CLIENT_ID')
GIGA_CHAT_AUTH_TOKEN = env('GIGA_CHAT_AUTH_TOKEN')
