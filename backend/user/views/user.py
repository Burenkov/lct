import re
from drf_spectacular.utils import extend_schema
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import viewsets
from decimal import Decimal
from ..serializers import UserSerializer
from django.contrib.auth import get_user_model
from ..models import UserCompany, Company, UserCompanyRole
from openpyxl import load_workbook
from datetime import datetime

from contract.models import Contract, Product as ProductContract
from product.models import Product as ProductProduct, TrialBalance
from warehouse.models import Product as ProductWarehouse, Category, Remains

User = get_user_model()

def validate_category(string):
    pattern = re.compile(r"^[0-9]+(.[0-9]+)?, .*$")
    return pattern.match(string) is not None

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()

    @extend_schema(summary='Получение данных текущего пользователя')
    @action(methods=['GET'], detail=False)
    def current(self, request) -> Response:
        serializer = self.get_serializer(request.user)
        return Response(serializer.data)

    @action(methods=['POST'], detail=False)
    def add_user(self, request) -> Response:
        company = Company.objects.first()
        role = UserCompanyRole.objects.first()
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        UserCompany.objects.create(
            user=user,
            company=company,
            role=role
        )
        return Response(serializer.data)

    @action(methods=['POST'], detail=False)
    def load_file(self, request) -> Response:
        type = request.data.get('type', None)
        file_obj = request.FILES.get('file', None)
        if file_obj is None:
            return Response({'detail': 'File not found'}, status=400)
        if type is None:
            return Response({'detail': 'Type not found'}, status=400)

        if type == 'load_contract':
            wb = load_workbook(file_obj)
            sheet = wb.active
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                id_spgz = obj_data[0]
                reestr_number = obj_data[2]
                if id_spgz is not None and id_spgz != '' and id_spgz != 'ID СПГЗ':
                    title = obj_data[1]
                    product, _ = ProductContract.objects.get_or_create(title=title)
                    final_code = obj_data[15]
                    tb = TrialBalance.objects.filter(product__ikz_code=final_code).first()
                    price = round(Decimal(0 if obj_data[11] is None or obj_data[11] == '' else obj_data[11]), 2)
                    count = 0
                    if tb is not None and price > 0 and tb.get_price_for_unit() > 0:
                        count = int(price / tb.get_price_for_unit())
                    obj, created = Contract.objects.update_or_create(
                        id_spgz=id_spgz,
                        reestr_number=reestr_number,
                        defaults={
                            'id_spgz': obj_data[0],
                            'title': obj_data[1],
                            'reestr_number': obj_data[2],
                            'lot_number': obj_data[3],
                            'ikz': obj_data[4],
                            'customer': obj_data[5],
                            'subject': obj_data[6],
                            'method': obj_data[7],
                            'basis': obj_data[8],
                            'status': obj_data[9],
                            'version': obj_data[10],
                            'price': round(Decimal(0 if obj_data[11] is None or obj_data[11] == '' else obj_data[11]), 2),
                            'price_conclusion': round(Decimal(0 if obj_data[12] is None or obj_data[12] == '' else obj_data[12]), 2),
                            'paid': round(Decimal(0 if obj_data[13] is None or obj_data[13] == '' else obj_data[13]), 2),
                            'paid_percent': round(float(0 if obj_data[14] is None or obj_data[14] == '' else obj_data[14]), 2),
                            'count': count,
                            'final_code': obj_data[15],
                            'final_name': obj_data[16],
                            'date_conclusion': datetime.strptime(obj_data[17], '%d.%m.%Y').date(),
                            'date_registration': datetime.strptime(obj_data[18], '%d.%m.%Y').date(),
                            'date_last_change': datetime.strptime(obj_data[19], '%d.%m.%Y').date(),
                            'date_start': datetime.strptime(obj_data[20], '%d.%m.%Y').date(),
                            'date_end': datetime.strptime(obj_data[21], '%d.%m.%Y').date(),
                            'date_end_conclusion': datetime.strptime(obj_data[22], '%d.%m.%Y').date(),
                            'supplier_type': obj_data[23],
                            'subject_rus': obj_data[24],
                            'law': obj_data[25],
                            'electronic_execution': obj_data[26],
                            'executed': obj_data[27],

                            'product': product
                        }
                    )
            return Response({ 'status': 'success' })
        if type == 'load_product':
            wb = load_workbook(file_obj)
            sheet = wb.active
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                title = obj_data[0]
                if title is not None and title != '' and title != 'Название СТЕ':
                    ProductProduct.objects.update_or_create(
                        title=obj_data[0],
                        defaults={
                            'property_names': obj_data[1],
                            'reference_price': Decimal(0 if obj_data[2] is None or obj_data[2] == 'NULL' else obj_data[2]),
                            'final_category': obj_data[3],
                            'ikz_code': obj_data[4],
                            'ikz': obj_data[5],
                            'spgz_code': obj_data[6],
                            'spgz': obj_data[7],
                            'reestr_number': obj_data[8],
                        }
                    )
            return Response({ 'status': 'success' })
        if type == 'load_trial_balance':
            wb = load_workbook(file_obj)
            sheet = wb.active
            string = file_obj.name
            # Регулярное выражение для извлечения информации
            pattern = r"сч\. (\d+) за (\d+) кв\. (\d+)г\."
            # Поиск совпадений с помощью re.search
            match = re.search(pattern, string)
            account = ''
            quarter = ''
            year = ''
            # Извлечение данных из совпадений
            if match:
                account = match.group(1)
                quarter = match.group(2)
                year = match.group(3)
            else:
                print("Не удалось найти информацию.")
            if account == '105':
                for row in sheet.iter_rows():
                    obj_data = []
                    for cell in row:
                        value = cell.value
                        if isinstance(value, str):
                            obj_data.append(cell.value.strip(" "))
                        else:
                            obj_data.append(cell.value)
                    code = obj_data[0]
                    if code is not None and code != '' and code != '№ п/п':
                        title = obj_data[3]
                        product = ProductProduct.objects.filter(title=title).first()
                        TrialBalance.objects.update_or_create(
                            code=code,
                            defaults={
                                'product': product,
                                'unit': obj_data[4] if obj_data[4] is not None else '',
                                'balance': Decimal(obj_data[6] if obj_data[6] is not None else 0),
                                'balance_count': obj_data[5] if obj_data[5] is not None else 0,
                                'balance_end': Decimal(obj_data[-1] if obj_data[-1] is not None else 0),
                                'balance_count_end': obj_data[-2] if obj_data[-2] is not None else 0,
                                'account': account,
                                'quarter': quarter,
                                'year': year,
                            }
                        )
            else:
                data = list(sheet.iter_rows())
                flat_data = []
                for row in data:
                    obj_data = []
                    for cell in row:
                        value = cell.value
                        if isinstance(value, str):
                            obj_data.append(cell.value.strip(" "))
                        else:
                            obj_data.append(cell.value)
                    flat_data.append(obj_data)
                i = 0
                for el in flat_data:
                    try:
                        if el[0] is None:
                            continue
                        product = ProductProduct.objects.filter(title=el[0]).first()
                        if product is None:
                            continue
                        next_i = i + 1
                        balance_count = flat_data[next_i][10]
                        balance_count_end = flat_data[next_i][-2]
                        code = f'{i}-{account}-{quarter}-{year}'
                        TrialBalance.objects.update_or_create(
                            code=code,
                            defaults={
                                'code': code,
                                'product': product,
                                'unit': '',
                                'balance': Decimal(obj_data[10] if obj_data[10] is not None else 0),
                                'balance_count': balance_count if balance_count is not None else 0,
                                'balance_end': Decimal(obj_data[-2] if obj_data[-2] is not None else 0),
                                'balance_count_end': balance_count_end if balance_count_end is not None else 0,
                                'account': account,
                                'quarter': quarter,
                                'year': year,
                            }
                        )
                        i += 1
                    except:
                        pass
            return Response({ 'status': 'success' })
        if type == 'load_statement_balances':
            date_pattern = r"(\d{2}\.\d{2}\.\d{4})"
            match = re.search(date_pattern, file_obj.name)
            if match:
                _date = match.group(1)
            else:
                _date = None
            balance_of_date = datetime.strptime(_date, '%d.%m.%Y').date()
            wb = load_workbook(file_obj)
            sheet = wb.active
            current_category = None
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if validate_category(str(value)):
                        category, created = Category.objects.get_or_create(title=value.strip())
                        current_category = category
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                number = obj_data[0]
                title = obj_data[2]
                inventory_number = obj_data[9]
                okof = '' if obj_data[10] is None else obj_data[10]
                if number is not None and number.replace(' ', '').isdigit() and title is not None and inventory_number is not None:
                    balance_value = Decimal(0) if obj_data[19] is None else Decimal(obj_data[19])
                    product, _ = ProductWarehouse.objects.get_or_create(title=title)
                    remains, created = Remains.objects.update_or_create(
                        inventory_number=inventory_number,
                        balance_of_date=balance_of_date,
                        # okof=okof,
                        defaults={
                            'title': obj_data[2],
                            'inventory_number': obj_data[9],
                            'okof': okof,
                            'depreciation_group': '' if obj_data[12] is None else obj_data[12],
                            'depreciation_method': str('' if obj_data[13] is None else obj_data[13]),
                            'date_acceptance_for_accounting': None if obj_data[14] is None else datetime.strptime(obj_data[14], '%d.%m.%Y').date(),
                            'condition': obj_data[15],
                            'useful_life': str('' if obj_data[16] is None else obj_data[16]),
                            'monthly_wear_rate': obj_data[17],
                            'wear': Decimal(0 if obj_data[18] is None or obj_data[18] == '-' else obj_data[18]),
                            'balance_value': balance_value,
                            'quantity': int(0 if obj_data[20] is None else obj_data[20]),
                            'amount_depreciation': Decimal(0 if obj_data[21] is None else obj_data[21]),
                            'residual_value': Decimal(0 if obj_data[22] is None else obj_data[22]),
                            'category': current_category,
                            'product': product,
                            'balance_of_date': balance_of_date
                        }
                    )
            return Response({ 'status': 'success' })
        return Response({ 'status': 'error' })
