# Generated by Django 3.2.25 on 2024-06-13 15:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Название')),
                ('full_title', models.CharField(max_length=255, verbose_name='Полное название')),
            ],
            options={
                'verbose_name': 'Компания',
                'verbose_name_plural': 'Компании',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='UserCompanyRole',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Название')),
                ('role_type', models.CharField(choices=[('admin', 'Админ'), ('owner', 'Владелец'), ('employee', 'Сотрудник')], default='employee', max_length=128, verbose_name='Тип')),
            ],
            options={
                'verbose_name': 'Роль в компании | User company role',
                'verbose_name_plural': 'Роли в компаниях | User companies roles',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='UserCompany',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Дата/время создания')),
                ('is_blocked', models.BooleanField(default=False, verbose_name='Заблокирован администратором компании')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.company')),
                ('role', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='user.usercompanyrole', verbose_name='Роль в компании')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Пользователь компании | Company user',
                'verbose_name_plural': 'Пользователи компании | Company users',
                'unique_together': {('user', 'company')},
            },
        ),
    ]
