from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin

from .models import UserCompany, Company, UserCompanyRole


class UserCompanyInline(admin.TabularInline):
    model = UserCompany
    extra = 0
    raw_id_fields = ['user']


@admin.register(UserCompany)
class UserCompanyAdmin(admin.ModelAdmin):
    list_display = ('user', 'company', 'role')
    list_filter = ('company', 'role')


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('title',)
    inlines = (UserCompanyInline,)


@admin.register(UserCompanyRole)
class UserCompanyRoleAdmin(admin.ModelAdmin):
    list_display = ('title', 'role_type')
    list_filter = ('role_type',)


@admin.register(User)
class UserAdmin(UserAdmin):
    fieldsets = (
        ('Viber', {
            'fields': (
                'viber_chat_id',
                'viber_secret_key',
            )
        }),
    ) + UserAdmin.fieldsets
