from rest_framework import serializers
from user.models import User
from ..models import UserCompany


class UserSerializer(serializers.ModelSerializer):

    company_name = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    def get_company_name(self, obj):
        uc = UserCompany.objects.filter(user=obj).first()
        if uc is None:
            return None
        
        return str(uc.company.title)
    
    def get_permissions(self, obj):
        uc = UserCompany.objects.filter(user=obj).first()
        if uc is None:
            return None
        
        return {
            'role': str(uc.role.title),
            'type': uc.role.role_type
        }

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'company_name',
            'permissions',
        )
