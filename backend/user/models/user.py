import uuid
from garpix_user.models import GarpixUser
from django.db import models

class User(GarpixUser):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, verbose_name='Уникальный идентификатор')

    def get_company(self):
        uc = self.usercompany_set.first()
        if uc is None:
            return None
        return uc.company

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username
