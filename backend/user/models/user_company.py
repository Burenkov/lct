from django.contrib.auth import get_user_model
from django.db import models
from .company import Company
from .user_role import UserCompanyRole


User = get_user_model()


class UserCompany(models.Model):
    """
    Модель участников. Связка между компанией и пользователем.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата/время создания')
    is_blocked = models.BooleanField(default=False, verbose_name='Заблокирован администратором компании')
    role = models.ForeignKey(UserCompanyRole, null=True, blank=True, on_delete=models.SET_NULL,
                             verbose_name='Роль в компании')


    class Meta:
        verbose_name = 'Пользователь компании | Company user'
        verbose_name_plural = 'Пользователи компании | Company users'
