from django.db import models


class UserCompanyRole(models.Model):
    """
    Роли в компаниях.
    """

    class ROLE_TYPE(models.TextChoices):
        ADMIN = ('admin', 'Админ')
        OWNER = ('owner', 'Владелец')
        EMPLOYEE = ('employee', 'Сотрудник')

    title = models.CharField(max_length=255, verbose_name='Название')
    role_type = models.CharField(max_length=128, choices=ROLE_TYPE.choices, default=ROLE_TYPE.EMPLOYEE,
                                 verbose_name='Тип')
    
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Роль в компании | User company role'
        verbose_name_plural = 'Роли в компаниях | User companies roles'
        ordering = ['-id']
