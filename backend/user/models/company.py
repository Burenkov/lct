from django.db import models


class Company(models.Model):
    """
    Данные о компании.
    """
    title = models.CharField(max_length=255, verbose_name='Название')
    full_title = models.CharField(max_length=255, verbose_name='Полное название')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'
        ordering = ['-id']
