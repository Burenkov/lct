from django.contrib import admin

from django.contrib import admin
from .models import Product, TrialBalance, Restriction


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    search_fields = (
        # 'title',
        'reestr_number',
    )


@admin.register(TrialBalance)
class TrialBalanceAdmin(admin.ModelAdmin):
    list_display = ('code', 'product',)


@admin.register(Restriction)
class RestrictionAdmin(admin.ModelAdmin):
    raw_id_fields = ('product1', 'product2',)
