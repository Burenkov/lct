# Generated by Django 3.2.25 on 2024-06-14 19:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_auto_20240614_2203'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trialbalance',
            name='turnover',
        ),
        migrations.RemoveField(
            model_name='trialbalance',
            name='turnover_count',
        ),
    ]
