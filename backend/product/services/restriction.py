from ..models import Restriction


def restriction_groups(products):
    """
    Проверяет ограничения на группировку товаров и возвращает список возможных групп.

    Args:
        products: Список объектов Product.

    Returns:
        Список списков объектов Product, представляющих возможные группы товаров.
        Возвращает пустой список, если никакие группы невозможны.
    """
    possible_groups = []
    for i in range(len(products)):
        current_group = [products[i]]
        for j in range(i+1, len(products)):
            product1 = products[i][0]
            product2 = products[j][0]
            if Restriction.objects.filter(product1=product1, product2=product2).exists() or \
               Restriction.objects.filter(product1=product2, product2=product1).exists():
                # Ограничение найдено, переходим к следующему товару
                continue
            current_group.append(products[j])  # Добавляем товар в текущую группу
        if current_group:  # Если группа не пуста, добавляем ее в список возможных групп
            possible_groups.append(current_group)

    return possible_groups
