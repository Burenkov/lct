import re
import os
from decimal import Decimal
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from openpyxl import load_workbook
from product.models import Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        full_path = os.path.realpath(__file__)
        
        path = os.path.dirname(full_path)
        files = [
            'КПГЗ ,СПГЗ, СТЕ.xlsx',
        ]
        for file in files:
            print(f'{path}/data/{file}')
            wb = load_workbook(f'{path}/data/{file}')
            sheet = wb.active
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                title = obj_data[0]
                if title is not None and title != '' and title != 'Название СТЕ':
                    Product.objects.update_or_create(
                        title=obj_data[0],
                        defaults={
                            'property_names': obj_data[1],
                            'reference_price': Decimal(0 if obj_data[2] is None or obj_data[2] == 'NULL' else obj_data[2]),
                            'final_category': obj_data[3],
                            'ikz_code': obj_data[4],
                            'ikz': obj_data[5],
                            'spgz_code': obj_data[6],
                            'spgz': obj_data[7],
                            'reestr_number': obj_data[8],
                        }
                    )

        self.stdout.write(
            self.style.SUCCESS('Successfully closed')
        )
