import re
import os
from decimal import Decimal
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from openpyxl import load_workbook
from product.models import TrialBalance, Product


class Command(BaseCommand):
    def handle(self, *args, **options):
        full_path = os.path.realpath(__file__)
        
        path = os.path.dirname(full_path)
        files = [
            'Оборотная ведомость по сч. 105 за 1 кв. 2022г..xlsx',
            'Оборотная ведомость по сч. 105 за 2 кв. 2022г..xlsx',
            'Оборотная ведомость по сч. 105 за 3 кв. 2022г..xlsx',
            'Оборотная ведомость по сч. 105 за 4 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 21 за 1 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 21 за 2 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 21 за 3 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 21 за 4 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 101 за 1 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 101 за 2 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 101 за 3 кв. 2022г..xlsx',
            'Оборотно-сальдовая ведомость по сч. 101 за 4 кв. 2022г..xlsx'
        ]
        for file in files:
            print(f'{path}/data/Обороты по счету/{file}')
            wb = load_workbook(f'{path}/data/Обороты по счету/{file}')
            sheet = wb.active
            string = file
            # Регулярное выражение для извлечения информации
            pattern = r"сч\. (\d+) за (\d+) кв\. (\d+)г\."
            # Поиск совпадений с помощью re.search
            match = re.search(pattern, string)
            account = ''
            quarter = ''
            year = ''
            # Извлечение данных из совпадений
            if match:
                account = match.group(1)
                quarter = match.group(2)
                year = match.group(3)

                print(f"Счет: {account}")
                print(f"Квартал: {quarter}")
                print(f"Год: {year}")
            else:
                print("Не удалось найти информацию.")
            if account == '105':
                for row in sheet.iter_rows():
                    obj_data = []
                    for cell in row:
                        value = cell.value
                        if isinstance(value, str):
                            obj_data.append(cell.value.strip(" "))
                        else:
                            obj_data.append(cell.value)
                    code = obj_data[0]
                    if code is not None and code != '' and code != '№ п/п':
                        title = obj_data[3]
                        product = Product.objects.filter(title=title).first()
                        TrialBalance.objects.update_or_create(
                            code=code,
                            defaults={
                                'product': product,
                                'unit': obj_data[4] if obj_data[4] is not None else '',
                                'balance': Decimal(obj_data[6] if obj_data[6] is not None else 0),
                                'balance_count': obj_data[5] if obj_data[5] is not None else 0,
                                'balance_end': Decimal(obj_data[-1] if obj_data[-1] is not None else 0),
                                'balance_count_end': obj_data[-2] if obj_data[-2] is not None else 0,
                                'account': account,
                                'quarter': quarter,
                                'year': year,
                            }
                        )
            else:
                data = list(sheet.iter_rows())
                flat_data = []
                for row in data:
                    obj_data = []
                    for cell in row:
                        value = cell.value
                        if isinstance(value, str):
                            obj_data.append(cell.value.strip(" "))
                        else:
                            obj_data.append(cell.value)
                    flat_data.append(obj_data)
                i = 0
                for el in flat_data:
                    try:
                        if el[0] is None:
                            continue
                        product = Product.objects.filter(title=el[0]).first()
                        if product is None:
                            continue
                        next_i = i + 1
                        balance_count = flat_data[next_i][10]
                        balance_count_end = flat_data[next_i][-2]
                        code = f'{i}-{account}-{quarter}-{year}'
                        TrialBalance.objects.update_or_create(
                            code=code,
                            defaults={
                                'code': code,
                                'product': product,
                                'unit': '',
                                'balance': Decimal(obj_data[10] if obj_data[10] is not None else 0),
                                'balance_count': balance_count if balance_count is not None else 0,
                                'balance_end': Decimal(obj_data[-2] if obj_data[-2] is not None else 0),
                                'balance_count_end': balance_count_end if balance_count_end is not None else 0,
                                'account': account,
                                'quarter': quarter,
                                'year': year,
                            }
                        )
                        i += 1
                    except:
                        pass

        self.stdout.write(
            self.style.SUCCESS('Successfully closed')
        )
