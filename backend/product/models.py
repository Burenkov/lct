from django.db import models

class Product(models.Model):
    title = models.CharField(max_length=2255, verbose_name='Название')
    property_names = models.TextField(verbose_name='Наименование характеристик', default='')
    reference_price = models.DecimalField(max_digits=15, decimal_places=2, default=0.0, verbose_name='Реф.цена')
    final_category = models.CharField(max_length=255, verbose_name='Конечную категорию справочника', default='')
    ikz_code = models.CharField(max_length=255, verbose_name='КПГЗ код', default='')
    ikz = models.CharField(max_length=255, verbose_name='КПГЗ', default='')
    spgz_code = models.CharField(max_length=255, verbose_name='СПГЗ код', default='')
    spgz = models.CharField(max_length=255, verbose_name='СПГЗ', default='')
    reestr_number = models.CharField(max_length=255, verbose_name='Реестровый номер в РК', default='')

    def get_property_names(self):
        return self.property_names.split(';')

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title
    

class TrialBalance(models.Model):
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='Продукт')
    code = models.CharField(max_length=255, verbose_name='Код справочника')
    unit = models.CharField(max_length=255, verbose_name='Единица измерения')
    balance = models.DecimalField(max_digits=15, decimal_places=2, default=0.0, verbose_name='Остаток')
    balance_count = models.IntegerField(default=0, verbose_name='Остаток количество')
    balance_end = models.DecimalField(max_digits=15, decimal_places=2, default=0.0, verbose_name='Остаток конец периода')
    balance_count_end = models.IntegerField(default=0, verbose_name='Остаток количество конец периода')
    account = models.CharField(max_length=255, verbose_name='Счет')
    quarter = models.CharField(max_length=255, verbose_name='Квартал')
    year = models.CharField(max_length=255, verbose_name='Год')

    def get_price_for_unit(self):
        if self.balance == 0:
            return 0
        return self.balance / self.balance_count

    class Meta:
        verbose_name = 'Баланс'
        verbose_name_plural = 'Балансы'

    def __str__(self):
        return str(self.pk)


class Restriction(models.Model):
    product1 = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='restriction_product1')
    product2 = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='restriction_product2')

    def __str__(self):
        return f"Товар {self.product1} не может быть объединен с товаром {self.product2}"

    class Meta:
        verbose_name = 'Ограничение'
        verbose_name_plural = 'Ограничения'