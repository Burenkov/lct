import inspect
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from chat.models import Message
from .product import Product

class Contract(models.Model):
    id_spgz = models.CharField(max_length=255, verbose_name="ID СПГЗ")
    title = models.CharField(max_length=1025, verbose_name="Наименование СПГЗ", default='', blank=True)
    reestr_number = models.CharField(max_length=255, verbose_name="Реестровый номер в РК", default='', blank=True)
    lot_number = models.CharField(max_length=255, verbose_name="Номер лота в закупке", default='', blank=True)
    ikz = models.CharField(max_length=255, verbose_name="ИКЗ", default='', blank=True)
    customer = models.CharField(max_length=255, verbose_name="Заказчик", default='', blank=True)
    subject = models.TextField(verbose_name="Наименование (предмет) ГК", default='', blank=True)
    method = models.CharField(max_length=255, verbose_name="Способ определения поставщика", default='', blank=True)
    basis = models.TextField(verbose_name="Основание заключения контракта с ед. поставщиком", default='', blank=True)
    status = models.CharField(max_length=255, verbose_name="Статус контракта", default='', blank=True)
    version = models.CharField(max_length=255, verbose_name="Номер версии", default='', blank=True)
    price = models.DecimalField(verbose_name="Цена ГК, руб.", max_digits=15, decimal_places=2, default=0.0, blank=True)
    price_conclusion = models.DecimalField(verbose_name="Цена ГК при заключении, руб.", max_digits=15, decimal_places=2, default=0.0, blank=True)
    paid = models.DecimalField(verbose_name="Оплачено, руб.", max_digits=15, decimal_places=2, default=0.0, blank=True)
    paid_percent = models.FloatField(verbose_name="Оплачено, %", default=0.0, blank=True)
    count = models.FloatField(verbose_name="Количество", default=0.0, blank=True)
    final_code = models.CharField(default='', blank=True, max_length=255, verbose_name="Конечный код КПГЗ")
    final_name = models.CharField(default='', blank=True, max_length=255, verbose_name="Конечное наименование КПГЗ")
    date_conclusion = models.DateField(default='', blank=True, verbose_name="Дата заключения")
    date_registration = models.DateField(default='', blank=True, verbose_name="Дата регистрации")
    date_last_change = models.DateField(default='', blank=True, verbose_name="Дата последнего изменения")
    date_start = models.DateField(default='', blank=True, verbose_name="Срок исполнения с")
    date_end = models.DateField(default='', blank=True, verbose_name="Срок исполнения по")
    date_end_conclusion = models.DateField(default='', blank=True, verbose_name="Дата окончания срока действия")
    supplier_type = models.CharField(default='', blank=True, max_length=255, verbose_name="Принадлежность поставщика к МСП на момент заключения ГК")
    subject_rus = models.CharField(default='', blank=True, max_length=255, verbose_name="Наименование субъекта РФ поставщика")
    law = models.CharField(default='', blank=True, max_length=255, verbose_name="Закон-основание (44/223)")
    electronic_execution = models.CharField(default='', blank=True, max_length=255, verbose_name="Электронное исполнение")
    executed = models.CharField(default='', blank=True, max_length=255, verbose_name="Исполнено поставщиком")

    product = models.ForeignKey(
        Product, 
        null=True, 
        blank=True, 
        on_delete=models.SET_NULL, 
        verbose_name='Продукт'
    )
    quantity = models.IntegerField(verbose_name='Количество', default=1)

    class Meta:
        verbose_name = "Контракт"
        verbose_name_plural = "Контракты"

    def __str__(self):
        return self.title

@receiver(pre_save, sender=Contract)
def contract_pre_save(sender, instance, **kwargs):
    if instance.pk is not None:
        old_obj = Contract.objects.get(pk=instance.pk)
        if old_obj.status != instance.status:
            obj = instance
            text = f"""
                Изменен статус договора:
                ID СПГЗ: {obj.id_spgz}
                Статус: был {old_obj.status} стал {obj.status}
                Наименование СПГЗ: {obj.title}
            """
            Message.objects.create(
                type=Message.Type.ALL,
                text=inspect.cleandoc(text),
                system_info={"contract_id": obj.id}
            )

@receiver(post_save, sender=Contract)
def contract_post_save(sender, instance, created, **kwargs):
    if created:
        obj = instance
        text = f"""
            Добавлен новый договор:
            ID СПГЗ: {obj.id_spgz}
            Статус: {obj.status}
            Наименование СПГЗ: {obj.title}
        """
        Message.objects.create(
            type=Message.Type.ALL,
            text=inspect.cleandoc(text),
            system_info={"contract_id": obj.id}
        )