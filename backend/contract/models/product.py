from django.db import models
from product.models import Product as ProductMain

class Product(models.Model):
    title = models.CharField(max_length=2255, verbose_name='Название')
    warehouse = models.ForeignKey('warehouse.Product', on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Продукт на складе')

    def get_id_spgz(self):
        contract = self.contract_set.first()
        if contract is None:
            return None
        return contract.id_spgz

    def get_main_product_for_spgz(self):
        contract = self.contract_set.first()
        if contract is None:
            return None
        final_code = contract.final_code
        print(self.title, final_code, 'reestr_number')
        return ProductMain.objects.filter(ikz_code=final_code).first()

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    def __str__(self):
        return self.title