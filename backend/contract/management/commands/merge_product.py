import re
import os
from decimal import Decimal
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from openpyxl import load_workbook
from ...models import Product as ProductContract
from product.models import Product as ProductMain
from warehouse.models import Product as ProductWarehouse
from chat.models import Message
from django.contrib.postgres.search import SearchVector, SearchRank, SearchQuery
from django.db.models import Q


class Command(BaseCommand):
    def handle(self, *args, **options):

        for product in ProductWarehouse.objects.all():
            filters = Q(search=SearchQuery(product.title))
            for word in product.title.split(" ")[:2]:
                filters |= Q(search=SearchQuery(word))
            vector = SearchVector("title")
            pc = ProductContract.objects.annotate(search=vector).filter(filters).exclude(warehouse__isnull=False).first()
            if pc is not None and pc.warehouse is None:
                print('-'*10)
                print(product.title)
                print(pc.title)
                pc.warehouse = product
                pc.save()

        self.stdout.write(
            self.style.SUCCESS('Successfully closed')
        )
