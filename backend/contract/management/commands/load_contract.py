import re
import os
from decimal import Decimal
from datetime import datetime
from django.core.management.base import BaseCommand, CommandError
from openpyxl import load_workbook
from ...models import Contract, Product
from chat.models import Message
from product.models import TrialBalance


class Command(BaseCommand):
    def handle(self, *args, **options):
        full_path = os.path.realpath(__file__)
        
        path = os.path.dirname(full_path)
        files = [
            'Выгрузка контрактов по Заказчику.xlsx',
        ]
        for file in files:
            print(f'{path}/data/{file}')
            wb = load_workbook(f'{path}/data/{file}')
            sheet = wb.active
            for row in sheet.iter_rows():
                obj_data = []
                for cell in row:
                    value = cell.value
                    if isinstance(value, str):
                        obj_data.append(cell.value.strip(" "))
                    else:
                        obj_data.append(cell.value)
                id_spgz = obj_data[0]
                reestr_number = obj_data[2]
                if id_spgz is not None and id_spgz != '' and id_spgz != 'ID СПГЗ':
                    title = obj_data[1]
                    product, _ = Product.objects.get_or_create(title=title)
                    final_code = obj_data[15]
                    tb = TrialBalance.objects.filter(product__ikz_code=final_code).first()
                    price = round(Decimal(0 if obj_data[11] is None or obj_data[11] == '' else obj_data[11]), 2)
                    count = 0
                    if tb is not None and price > 0 and tb.get_price_for_unit() > 0:
                        count = int(price / tb.get_price_for_unit())
                    obj, created = Contract.objects.update_or_create(
                        id_spgz=id_spgz,
                        reestr_number=reestr_number,
                        defaults={
                            'id_spgz': obj_data[0],
                            'title': obj_data[1],
                            'reestr_number': obj_data[2],
                            'lot_number': obj_data[3],
                            'ikz': obj_data[4],
                            'customer': obj_data[5],
                            'subject': obj_data[6],
                            'method': obj_data[7],
                            'basis': obj_data[8],
                            'status': obj_data[9],
                            'version': obj_data[10],
                            'price': round(Decimal(0 if obj_data[11] is None or obj_data[11] == '' else obj_data[11]), 2),
                            'price_conclusion': round(Decimal(0 if obj_data[12] is None or obj_data[12] == '' else obj_data[12]), 2),
                            'paid': round(Decimal(0 if obj_data[13] is None or obj_data[13] == '' else obj_data[13]), 2),
                            'paid_percent': round(float(0 if obj_data[14] is None or obj_data[14] == '' else obj_data[14]), 2),
                            'count': count,
                            'final_code': obj_data[15],
                            'final_name': obj_data[16],
                            'date_conclusion': datetime.strptime(obj_data[17], '%d.%m.%Y').date(),
                            'date_registration': datetime.strptime(obj_data[18], '%d.%m.%Y').date(),
                            'date_last_change': datetime.strptime(obj_data[19], '%d.%m.%Y').date(),
                            'date_start': datetime.strptime(obj_data[20], '%d.%m.%Y').date(),
                            'date_end': datetime.strptime(obj_data[21], '%d.%m.%Y').date(),
                            'date_end_conclusion': datetime.strptime(obj_data[22], '%d.%m.%Y').date(),
                            'supplier_type': obj_data[23],
                            'subject_rus': obj_data[24],
                            'law': obj_data[25],
                            'electronic_execution': obj_data[26],
                            'executed': obj_data[27],

                            'product': product
                        }
                    )
                    # datetime.strptime(obj_data[14], '%d.%m.%Y').date()

        self.stdout.write(
            self.style.SUCCESS('Successfully closed')
        )
