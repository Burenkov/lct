from django.contrib import admin
from ..models import Contract

@admin.register(Contract)
class ContractAdmin(admin.ModelAdmin):
    search_fields = [
        'id_spgz',
        'title',
        'reestr_number',
        'lot_number',
        'ikz',
        'customer',
        'subject',
        'final_code'
        # 3578723
    ]

    list_display = [
        'title',
        'count',
        'final_code'
    ]
