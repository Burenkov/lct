from rest_framework import viewsets
from rest_framework import filters
from ..models import Contract
from ..serializers import ContractSerializer


class ContractViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['title']
