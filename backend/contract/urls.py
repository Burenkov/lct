from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'', views.ContractViewSet, basename='contract')

urlpatterns = router.urls
